package OOP.Animal;

public abstract class Animal {
    abstract protected String makeSound();
    public String name;

    public Animal(String name) {
        this.name = name;
        System.out.println("Hello! I'm " +this.name+ "!");
    }
}
