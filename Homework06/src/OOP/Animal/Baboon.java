package OOP.Animal;

public class Baboon extends Monkey {
    public Baboon(String name, String action) {
        super(name, action);
    }

    @Override
    public String makeSound()
    {
        return "I " +this.action+ "!";
    }
}
