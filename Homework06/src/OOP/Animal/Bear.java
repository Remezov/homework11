package OOP.Animal;

public abstract class Bear extends Animal {
    public Bear(String name) {
        super(name);
    }
    public String call()
    {
        return this.name+" said: "+this.makeSound();
    }
}
