package ru.remezov.animal;

public class Dog extends Animal implements Running {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void getName() {
        System.out.println("I'm " +this.name+ "!");
    }
}
