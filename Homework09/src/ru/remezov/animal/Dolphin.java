package ru.remezov.animal;

public class Dolphin extends Animal implements Swimming{

    public Dolphin(String name) {
        super(name);
    }

    @Override
    public void getName() {
        System.out.println("I'm " +this.name+ "!");
    }
}
