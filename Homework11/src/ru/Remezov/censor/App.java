package ru.Remezov.censor;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите строку");
        String text = s.nextLine();

        /*подключаем CASE_INSENSITIVE и UNICODE_CASE*/

        Pattern pattern = Pattern.compile("(?iu)\\bбяка\\b");
        Matcher matcher = pattern.matcher(text);
        System.out.println(matcher.replaceAll("*вырезано цензурой*"));
    }
}
