package Comparison;

public class Main {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        if (a > b) {
            System.out.printf("%d less than %d", b, a);
        } else {
            System.out.printf("%d less than %d", a, b);
        }
    }
}
